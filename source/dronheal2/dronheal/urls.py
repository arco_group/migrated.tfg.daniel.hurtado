from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from mapa.views import mapa, operacion, add_envio

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
	url(r'^$', login, {'template_name': 'login.html'}, name="login"),
    url(r'^mapa/', mapa, name="mapa"),
    url(r'^operacion/', operacion, name="operacion"),
    url(r'^enviar/', add_envio, name='padd'),
    url(r'^logout$', logout, {'template_name': 'logged_out.html'}, name="logout"),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
