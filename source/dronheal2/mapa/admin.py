from django.contrib import admin
from mapa.models import Central, Cliente, Drone, Envio

# Register your models here.

class EnvioAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {'fields':['cliente']}),
		(None, {'fields':['drone']}),
		('Fecha de Envio', {'fields':['fecha_creacion'], 'classes':['collapse']}),]

admin.site.register(Central)
admin.site.register(Cliente)
admin.site.register(Drone)
admin.site.register(Envio, EnvioAdmin)
