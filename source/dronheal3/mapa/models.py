#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models, connections

for conn in connections.all():
    if conn.vendor == 'sqlite':
        conn.allow_thread_sharing = True

# Create your models here.

