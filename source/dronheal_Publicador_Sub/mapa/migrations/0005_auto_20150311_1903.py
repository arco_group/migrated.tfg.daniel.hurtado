# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0004_auto_20150309_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 11, 19, 3, 48, 598700), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
