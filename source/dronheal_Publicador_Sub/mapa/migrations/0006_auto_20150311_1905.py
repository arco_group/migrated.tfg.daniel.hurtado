# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0005_auto_20150311_1903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 11, 19, 5, 51, 755892), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
