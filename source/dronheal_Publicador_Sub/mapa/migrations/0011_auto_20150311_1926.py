# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0010_auto_20150311_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 11, 19, 26, 46, 103254), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
