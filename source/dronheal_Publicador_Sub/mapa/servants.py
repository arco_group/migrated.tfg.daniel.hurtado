# -*- mode: python; coding: utf-8 -*-

import time, sys
import thread, threading
import mavproxyapi
import wise

modulo = mavproxyapi.mpstate.modules[-1][0]
conexion = modulo.get_connection()
vehiculo = conexion.get_vehicles()[0]

def send_messages(observable):
    vehiculo.wait_init()
    observable.notify("set_message", "Conectando con el drone ...")
    time.sleep(20)
    observable.notify("set_message", "Conectado")
    while True:
        time.sleep(1)
        posicion = formatGPS()
        observable.notify("set_message", posicion)
#        print (posicion)

def formatGPS():
    location = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(vehiculo.location.lat, vehiculo.location.lon, vehiculo.location.alt)
    return location

def send_position(observable):
    vehiculo.wait_init()
    observable.notify("set_message", "Conectando con el drone ...")
    time.sleep(20)
    observable.notify("set_message", "Conectado")
    while True:
        time.sleep(1)
        lat = vehiculo.location.lat
        lon = vehiculo.location.lon
        alt = vehiculo.location.alt
        observable.notify("set_lat", lat)
        observable.notify("set_lon", lon)
        observable.notify("set_alt", alt)

def initialize(broker):
    _observable = wise.Observable()
    adapter = broker.createObjectAdapter("Adapter", "-w ws1")
    adapter.add(_observable, "Observable")
    time.sleep(5)
#    t = threading.Timer(1.0, send_messages, (_observable,))
#    t.start()
    thread.start_new_thread(send_position, (_observable,))
