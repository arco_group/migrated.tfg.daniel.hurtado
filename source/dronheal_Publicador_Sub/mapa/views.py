#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.core.urlresolvers import reverse
from mapa.models import *
from django.views.generic import ListView

# Create your views here.

class EnvioList(ListView):

	model = Envio


def index(request):
	clientes=Cliente.objects.order_by('nombre')
	drones=Drone.objects.order_by('nombre')
	central=Central.objects.order_by('nombre')
	form = EnvioForm()
	return render_to_response('mapa.html', {'form':form,'clientes': clientes,'drones': drones,'central':central})

def add_envio(request):
	#Si el usuario esta mandando el formulario con los datos
	if request.method == 'POST':
		form = EnvioForm(request.POST)
		#Comprobamos que el formulario es correcto
		if form.is_valid():
			#Guardamos los datos en la base de datos
			new_envio = form.save()

			return HttpResponseRedirect(reverse('index'))
	else:
		form = EnvioForm()

	return render(request, 'mapa/envio_form.html', {'form':form})

