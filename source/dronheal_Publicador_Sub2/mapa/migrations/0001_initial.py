# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Central',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=25)),
                ('calle', models.CharField(max_length=20)),
                ('numero', models.IntegerField(max_length=3)),
                ('codigo', models.IntegerField(default=1300, max_length=5)),
                ('ciudad', models.CharField(max_length=25)),
                ('pais', models.CharField(default=b'Espa\xc3\xb1a', max_length=10)),
                ('coorX', models.FloatField(max_length=10)),
                ('coorY', models.FloatField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40)),
                ('calle', models.CharField(max_length=25)),
                ('numero', models.IntegerField(max_length=3)),
                ('codigo', models.IntegerField(default=1300, max_length=5)),
                ('ciudad', models.CharField(default=b'Ciudad Real', max_length=25)),
                ('pais', models.CharField(default=b'Espa\xc3\xb1a', max_length=10)),
                ('coorX', models.FloatField(max_length=10)),
                ('coorY', models.FloatField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Drone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=20)),
                ('estado', models.IntegerField(default=1, max_length=1)),
                ('coorX', models.FloatField(max_length=10)),
                ('coorY', models.FloatField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Envio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_creacion', models.DateTimeField(verbose_name=b'fecha de envio')),
                ('cliente', models.ForeignKey(to='mapa.Cliente')),
                ('drone', models.ForeignKey(to='mapa.Drone')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
