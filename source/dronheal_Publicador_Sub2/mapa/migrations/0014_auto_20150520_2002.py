# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0013_auto_20150408_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 20, 20, 2, 44, 749851), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
