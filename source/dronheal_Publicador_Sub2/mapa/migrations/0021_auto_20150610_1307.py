# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0020_auto_20150610_1303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 10, 13, 7, 25, 952181), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
