# -*- mode: python; coding: utf-8 -*-

from droneapi.lib import VehicleMode, Location
import time, sys
import thread, threading
import mavproxyapi
import wise

modulo = mavproxyapi.mpstate.modules[-1][0]
conexion = modulo.get_connection()
vehiculo = conexion.get_vehicles()[0]
ALTITUDVUELO = 20
_observable = wise.Observable()


def despegar(latorigen, lonorigen):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latorigen, lonorigen, ALTITUDVUELO, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def viajar(latdestino, londestino):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return

		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, ALTITUDVUELO, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def entregar(latdestino, londestino):
	altitude = 2
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, altitude, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def aterrizar(latdestino, londestino):
	altitude = 0
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, altitude, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def Pedido(latorigen, lonorigen, latdestino, londestino):
	despegar(latorigen, lonorigen)
	_observable.notify("set_message", "Despegando...")
	viajar(latdestino, londestino)
	_observable.notify("set_message", "Viajando al destino...")
	entregar(latdestino, londestino)
	_observable.notify("set_message", "Bajando para la entrega...")
	viajar(latorigen, lonorigen)
	_observable.notify("set_message", "Volviendo...")
	aterrizar(latorigen, lonorigen)
	_observable.notify("set_message", "Aterrizando...")
	_observable.notify("set_message", "Pedido realiazado correctamente")

def send_position(observable):
    vehiculo.wait_init()
    observable.notify("set_message", "Conectando con el drone ...")
    time.sleep(20)
    observable.notify("set_message", "Conectado")
    while True:
        time.sleep(1)
        lat = vehiculo.location.lat
        lon = vehiculo.location.lon
        alt = vehiculo.location.alt
        observable.notify("set_coor", lon, lat)
        observable.notify("set_locatorScreen", alt)

def initialize(broker):

    adapter = broker.createObjectAdapter("Adapter", "-w ws1")
    adapter.add(_observable, "Observable")
    time.sleep(5)
    thread.start_new_thread(send_position, (_observable,))
