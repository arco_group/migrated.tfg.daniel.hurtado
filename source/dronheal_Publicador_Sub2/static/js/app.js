// -*- mode: js; coding: utf-8 -*-

function prepend(id, text) {
    div = document.getElementById(id);
    div.innerHTML = text + div.innerHTML;
    }

Servant = Class.extend({
    set_message: function(message) {
        prepend('messages', message + '<br/>');
        },
    });

function wise_application(broker) {
    broker.createObjectAdapter("WebAdapter", "-w browser")
        .then(on_adapter_ready);
    function on_adapter_ready(adapter) {
        adapter.add(new Servant(), "printer1");
        broker.ready();
        }
    }
