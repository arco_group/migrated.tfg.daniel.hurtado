# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0012_auto_20150313_1638'),
    ]

    operations = [
        migrations.RenameField(
            model_name='central',
            old_name='coorX',
            new_name='lat',
        ),
        migrations.RenameField(
            model_name='central',
            old_name='coorY',
            new_name='lon',
        ),
        migrations.RenameField(
            model_name='cliente',
            old_name='coorX',
            new_name='lat',
        ),
        migrations.RenameField(
            model_name='cliente',
            old_name='coorY',
            new_name='lon',
        ),
        migrations.RenameField(
            model_name='drone',
            old_name='coorX',
            new_name='lat',
        ),
        migrations.RenameField(
            model_name='drone',
            old_name='coorY',
            new_name='lon',
        ),
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 8, 16, 46, 17, 224420), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
