# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0016_auto_20150525_1915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 25, 20, 11, 11, 399716), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
