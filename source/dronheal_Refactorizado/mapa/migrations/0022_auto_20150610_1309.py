# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0021_auto_20150610_1307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 10, 13, 9, 1, 881504), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
