# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0022_auto_20150610_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 11, 20, 6, 50, 422946), verbose_name=b'fecha_envio'),
            preserve_default=True,
        ),
    ]
