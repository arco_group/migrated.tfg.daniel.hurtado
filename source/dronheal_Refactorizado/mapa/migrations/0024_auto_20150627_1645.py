# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0023_auto_20150611_2006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 27, 16, 45, 15, 852182), verbose_name=b'fecha_envio'),
            preserve_default=True,
        ),
    ]
