#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mision.py
#  
#  Copyright 2015 DanielH <danicianuro@baran>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class Mision():
	def __init__(self, latO, lonO, latD, lonD, alt, altE):
		self.wp = []
		self.latorigen = float(latO)
		self.lonorigen = float(lonO)
		self.latdestino = float(latD)
		self.londestino = float(lonD)
		self.altitud = float(alt)
		self.altitudentrega = float(altE)

	def hacer_ruta(self):
		self.wp.append("QGC WPL 110\n")
		self.wp.append("0	1	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t0.000000\t1\n")
		#mision de despegue
		self.wp.append("1	0	0	22	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t" + str(altitud) + "00000\t1\n")
		#mision ir a destino
		self.wp.append("2	0	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latdestino) + "\t" + str(londestino) + "\t" + str(altitud) + "00000\t1\n")
		#mision cambiar altitud
		self.wp.append("3	0	0	30	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000\t" + str(altitudentrega) + "00000\t1\n")
		#mision sensor pwm
		self.wp.append("4	0	0	183	2.000000	1500.000	0.000000	0.000000	0.000000	0.000000	0.000000	1\n")
		self.wp.append("5	0	0	183	2.000000	1000.000	0.000000	0.000000	0.000000	0.000000	0.000000	1\n")
		self.wp.append("6	0	0	30	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000\t" + str(altitud) + "00000\t1\n")
		self.wp.append("7	0	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t" + str(altitud) + "00000\t1\n")
		#mision aterrizar
		self.wp.append("8	0	0	21	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t0.000000\t1\n")

	def crear_archivo(self, nombre):
		f = open(nombre,"w")
		for i in range(10):
			f.write(self.wp[i])
		f.close()


