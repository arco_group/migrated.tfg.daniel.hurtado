# -*- mode: python; coding: utf-8 -*-

from droneapi.lib import VehicleMode, Location
import time, sys, socket
import thread, threading
import mavproxyapi, mision
import wise

ALTITUDVUELO = 20
ALTITUDENTREGA = 2

modulo = mavproxyapi.mpstate.modules[-1][0]
conexion = modulo.get_connection()
vehiculo = conexion.get_vehicles()[0]
_observable = wise.Observable()


def despegar(latorigen, lonorigen):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latorigen, lonorigen, ALTITUDVUELO, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def viajar(latdestino, londestino):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return

		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, ALTITUDVUELO, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def entregar(latdestino, londestino):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, ALTITUDENTREGA, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def aterrizar(latdestino, londestino):
	try:
		if vehiculo.mode.name == "INITIALISING":
			print "Vehiculo todavia está arrancando, intentalo más tarde"
			return
		
		cmds = vehiculo.commands
		vehiculo.mode = VehicleMode("GUIDED")
		dest = Location(latdestino, londestino, 0, is_relative=True)
		cmds.goto(dest)
		vehiculo.flush()
		time.sleep(10)
			
	except socket.error:
		print "Error: no puede acceder a los datos del GPS"

def Pedido(latorigen, lonorigen, latdestino, londestino):
	despegar(latorigen, lonorigen)
	_observable.notify("set_message", "Despegando...")
	viajar(latdestino, londestino)
	_observable.notify("set_message", "Viajando al destino...")
	entregar(latdestino, londestino)
	_observable.notify("set_message", "Bajando para la entrega...")
	viajar(latorigen, lonorigen)
	_observable.notify("set_message", "Volviendo...")
	aterrizar(latorigen, lonorigen)
	_observable.notify("set_message", "Aterrizando...")
	_observable.notify("set_message", "Pedido realiazado correctamente")

def crear_mision(latorigen, lonorigen, latdestino, londestino):
	mision = Mision(latorigen, lonorigen, latdestino, londestino, ALTITUDVUELO, ALTITUDENTREGA)
	mision.hacer_ruta()
	mision.crear_archivo()

	print "Generando %s waypoints..." % len(mision.wp)
	cmds = vehiculo.commands
	cmds.clear()
	for i in xrange(0, len(mision.wp)):
		cmds.add(mision.wp[i])

	vehiculo.flush()

	if vehiculo.mode.name == "INITIALISING":
		print "Esperando a que el vehiculo arranque"
		time.sleep(1)

	while vehiculo.gps_0.fix_type < 2:
		print "Esperando al GPS...:", vehiculo.gps_0.fix_type
		time.sleep(1)

	vehiculo.mode    = VehicleMode("AUTO")
	vehiculo.armed   = True
	vehiculo.flush()

def send_position(observable):
    vehiculo.wait_init()
    observable.notify("set_message", "Conectando con el drone ...")
    time.sleep(20)
    observable.notify("set_message", "Conectado")
    while True:
        time.sleep(1)
        lat = vehiculo.location.lat
        lon = vehiculo.location.lon
        alt = vehiculo.location.alt
        observable.notify("set_coor", lon, lat)
        observable.notify("set_locatorScreen", alt)

def initialize(broker):

    adapter = broker.createObjectAdapter("Adapter", "-w ws1")
    adapter.add(_observable, "Observable")
    time.sleep(5)
    thread.start_new_thread(send_position, (_observable,))
