#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models, connections
from django.forms import ModelForm
import datetime

for conn in connections.all():
    if conn.vendor == 'sqlite':
        conn.allow_thread_sharing = True

# Create your models here.

class Central(models.Model):
	nombre = models.CharField(max_length=25)
	calle = models.CharField(max_length=20)
	numero = models.IntegerField(max_length=3)
	codigo = models.IntegerField(max_length=5,default=1300)
	ciudad = models.CharField(max_length=25)
	pais = models.CharField(max_length=10,default='España')
	lat = models.FloatField(max_length=10)
	lon = models.FloatField(max_length=10)

	def __unicode__(self):
		return self.nombre

class Cliente(models.Model):
	nombre = models.CharField(max_length=40)
	calle = models.CharField(max_length=25)
	numero = models.IntegerField(max_length=3)
	codigo = models.IntegerField(max_length=5,default=1300)
	ciudad = models.CharField(max_length=25,default='Ciudad Real')
	pais = models.CharField(max_length=10,default='España')
	lat = models.FloatField(max_length=10)
	lon = models.FloatField(max_length=10)

	def __unicode__(self):
		return self.nombre

class Drone(models.Model):
	nombre = models.CharField(max_length=20)
	estado = models.IntegerField(max_length=1,default=1)
	lat = models.FloatField(max_length=10)
	lon = models.FloatField(max_length=10)

	def __unicode__(self):
		return self.nombre

class Envio(models.Model):
	cliente = models.ForeignKey(Cliente)
	drone = models.ForeignKey(Drone)
	fecha_creacion = models.DateTimeField('fecha de envio',default=datetime.datetime.now())

	def __unicode__(self):
		return u'%s - %s - %s' % (self.fecha_creacion, self.cliente, self.drone)

class EnvioForm(ModelForm):
	class Meta:
		model = Envio
		fields = ['cliente', 'drone']
