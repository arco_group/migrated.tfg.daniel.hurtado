# -*- mode: python; coding: utf-8 -*-

import time, sys
import thread
import mavproxyapi

modulo = mavproxyapi.mpstate.modules[-1][0]
conexion = modulo.get_connection()
vehiculo = conexion.get_vehicles()[0]


def get_proxy(broker):
    while 1:
        retval = broker.stringToProxy("printer1 -w browser")
        if retval:
            return retval

        print("Navegador no esta listo...")
        time.sleep(1)

def send_messages(broker):
    proxy = get_proxy(broker)
    vehiculo.wait_init()
    proxy.set_message("Conectando con el drone ...")
    for i in range(100):
        if i == 12:
            proxy.set_message("Conectado")
        if i > 12:
            posicion = enviarGPS()
            proxy.set_message(posicion + "  ({})".format(i))
        time.sleep(1)

def enviarGPS():
    location = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(vehiculo.location.lat, vehiculo.location.lon, vehiculo.location.alt)
    return location

def initialize(broker):

    thread.start_new_thread(send_messages, (broker,))
