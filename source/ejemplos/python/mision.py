#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mision.py
#  
#  Copyright 2015 DanielH <danicianuro@baran>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  



def main():
	wp = []

	print "Introduce las coordenadas del origen"
	latorigen = float(input("Introduce la latitud: "))
	lonorigen = float(input("Introduce la longitud: "))
	print "Introduce las coordenadas del destino"
	latdestino = float(input("Introduce la latitud: "))
	londestino = float(input("Introduce la longitud: "))
	altitud = float(input("Introduce la altitud del envio: "))
	altitudentrega = float(input("Introduce la altitud de la entrega: "))

	cabecera = "QGC WPL 110\n"
	wp.append("0	1	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t0.000000\t1\n")
	#mision de despegue
	wp.append("1	0	0	22	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t" + str(altitud) + "00000\t1\n")
	#mision ir a destino
	wp.append("2	0	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latdestino) + "\t" + str(londestino) + "\t" + str(altitud) + "00000\t1\n")
	#mision cambiar altitud
	wp.append("3	0	0	30	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000\t" + str(altitudentrega) + "00000\t1\n")
	#mision sensor pwm
	wp.append("4	0	0	183	2.000000	1500.000	0.000000	0.000000	0.000000	0.000000	0.000000	1\n")
	wp.append("5	0	0	183	2.000000	1000.000	0.000000	0.000000	0.000000	0.000000	0.000000	1\n")
	wp.append("6	0	0	30	0.000000	0.000000	0.000000	0.000000	0.000000	0.000000\t" + str(altitud) + "00000\t1\n")
	wp.append("7	0	0	16	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t" + str(altitud) + "00000\t1\n")
	#mision aterrizar
	wp.append("8	0	0	21	0.000000	0.000000	0.000000	0.000000\t" + str(latorigen) + "\t" + str(lonorigen) + "\t0.000000\t1\n")

	f = open("mision.txt","w")
	f.write(cabecera)
	for i in range(9):
		f.write(wp[i])
	f.close()
	return 0

if __name__ == '__main__':
	main()

