#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  run_gunicorn.py
#  
#  Copyright 2015 DaniCianuro <dani@cianuro.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys, os, django
from django.conf import settings

#settings.configure()
#os.environ.setdefault("PATH", "/home/danicianuro/Dropbox/tfg.daniel.hurtado/source/dronheal/")
os.environ.setdefault("PYTHONPATH", "/home/danicianuro/Dropbox/tfg.daniel.hurtado/source/dronheal/")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dronheal.settings")
#os.environ.setdefault("ROOT_URLCONF", "dronheal.urls")

WORKER_CLASS = 'wise.guworkers.GeventWebSocketWorker'
WSGI_APPLICATION = 'dronheal.wsgi:application'

cadena = "gunicorn \
    --debug \
    --reload \
    --access-logfile - \
    --error-logfile - \
    --log-level info \
    --worker-class " + WORKER_CLASS + " " + WSGI_APPLICATION

os.system(cadena)
