#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  servants.py
#  
#  Copyright 2015 DaniCianuro <dani@cianuro.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from droneapi.lib import VehicleMode, local_connect
from pymavlink import mavutil
import wise, time


def get_proxy(broker):
    while 1:
        retval = broker.stringToProxy("obj1 -w browser")
        if retval:
            return retval
        
        print ("Browser in not ready...")
        time.sleep(1)

    return self.broker.stringToProxy_wait

def send_messages(broker, v):
    proxy = get_proxy(broker)
    while True:
        cadena = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(v.location.lat, v.location.lon, v.location.alt)
        response = proxy.upper(cadena)
        time.sleep(1)

def initialize(broker):
    # First get an instance of the API endpoint
    api = local_connect()
    # get our vehicle - when running with mavproxy it only knows about one vehicle (for now)
    v = api.get_vehicles()[0]
    print "Esperando a que se inicie el dron.."
    v.wait_init()
    print "Dron Activado"       
    send_messages(broker, v)

