#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  servants.py
#  
#  Copyright 2015 DaniCianuro <dani@cianuro.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class MyServant(object):
    def __init__(self):
#        self.broker = wise.initialize(None, None, None, 'gunicorn')
        self.broker = wise.initialize(server="gunicorn")
#        self.broker.register_StaticFS('/operacion/', '/home/danicianuro/Dropbox/tfg.daniel.hurtado/source/dronheal/static')
#        print("Open: {}".format(self.broker.get_url('operacion/index2.html')))
        time.sleep(5)
        self.send_messages()

    def get_proxy(self):
        while 1:
            retval = self.broker.stringToProxy("obj1 -w browser")
            if retval:
                return retval

            print("Browser is not ready...")
            time.sleep(1)

        return self.broker.stringToProxy_wait

    def send_messages(self):
        proxy = self.get_proxy()
        while True:
            cadena = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(v.location.lat, v.location.lon, v.location.alt)
            response = proxy.upper(cadena)
            time.sleep(1)
	
	
	
    def sayHello(self, current):
        print "Hello!"


def initialize(broker):
    adapter = broker.createObjectAdapter("Adapter", "-w hello")
    adapter.add(MyServant(), "Hello")

