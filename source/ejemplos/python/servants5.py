#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  servants5.py
#  
#  Copyright 2015 DaniCianuro <dani@cianuro.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys, os, time, socket, signal
import fnmatch, errno, threading
import serial, Queue, select
import traceback
import select
import wise

from MAVProxy.modules.lib import textconsole
from MAVProxy.modules.lib import rline
from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import dumpstacks

from droneapi.lib import VehicleMode, local_connect
from pymavlink import mavutil

try:
    from multiprocessing import freeze_support
    from pymavlink import mavwp, mavutil
    import wx, matplotlib, HTMLParser
    try:
          import readline
    except ImportError:
          import pyreadline as readline
except Exception:
    pass

if __name__ == '__main__':
    freeze_support()

class MPStatus(object):
    '''hold status information about the mavproxy'''
    def __init__(self):
        self.gps	 = None
        self.msgs = {}
        self.msg_count = {}
        self.counters = {'MasterIn' : [], 'MasterOut' : 0, 'FGearIn' : 0, 'FGearOut' : 0, 'Slave' : 0}
        self.setup_mode = opts.setup
        self.mav_error = 0
        self.altitude = 0
        self.last_altitude_announce = 0.0
        self.last_distance_announce = 0.0
        self.exit = False
        self.flightmode = 'MAV'
        self.last_mode_announce = 0
        self.logdir = None
        self.last_heartbeat = 0
        self.last_message = 0
        self.heartbeat_error = False
        self.last_apm_msg = None
        self.last_apm_msg_time = 0
        self.highest_msec = 0
        self.have_gps_lock = False
        self.lost_gps_lock = False
        self.last_gps_lock = 0
        self.watch = None
        self.last_streamrate1 = -1
        self.last_streamrate2 = -1
        self.last_seq = 0
        self.armed = False

    def show(self, f, pattern=None):
        '''write status to status.txt'''
        if pattern is None:
            f.write('Counters: ')
            for c in self.counters:
                f.write('%s:%s ' % (c, self.counters[c]))
            f.write('\n')
            f.write('MAV Errors: %u\n' % self.mav_error)
            f.write(str(self.gps)+'\n')
        for m in sorted(self.msgs.keys()):
            if pattern is not None and not fnmatch.fnmatch(str(m).upper(), pattern.upper()):
                continue
            f.write("%u: %s\n" % (self.msg_count[m], str(self.msgs[m])))

    def write(self):
        '''write status to status.txt'''
        f = open('status.txt', mode='w')
        self.show(f)
        f.close()


def say_text(text, priority='important'):
    '''text output - default function for say()'''
    mpstate.console.writeln(text)

def say(text, priority='important'):
    '''text and/or speech output'''
    mpstate.functions.say(text, priority)

def add_input(cmd, immediate=False):
    '''add some command input to be processed'''
    if immediate:
        process_stdin(cmd)
    else:
        mpstate.input_queue.put(cmd)


class MAVFunctions(object):
    '''core functions available in modules'''
    def __init__(self):
        self.process_stdin = add_input
        self.param_set = param_set
        self.get_mav_param = get_mav_param
        self.say = say_text


class MPState(object):
    '''holds state of mavproxy'''
    def __init__(self):
        self.console = textconsole.SimpleConsole()
        self.map = None
        self.map_functions = {}
        self.vehicle_type = None
        self.vehicle_name = None
        from MAVProxy.modules.lib.mp_settings import MPSettings, MPSetting
        self.settings = MPSettings(
            [ MPSetting('link', int, 1, 'Primary Link', tab='Link', range=(0,4), increment=1),
              MPSetting('streamrate', int, 4, 'Stream rate link1', range=(-1,20), increment=1),
              MPSetting('streamrate2', int, 4, 'Stream rate link2', range=(-1,20), increment=1),
              MPSetting('heartbeat', int, 1, 'Heartbeat rate', range=(0,5), increment=1),
              MPSetting('mavfwd', bool, True, 'Allow forwarded control'),
              MPSetting('mavfwd_rate', bool, False, 'Allow forwarded rate control'),
              MPSetting('shownoise', bool, True, 'Show non-MAVLink data'),
              MPSetting('baudrate', int, opts.baudrate, 'baudrate for new links', range=(0,10000000), increment=1),
              MPSetting('rtscts', bool, opts.rtscts, 'enable flow control'),
              MPSetting('select_timeout', float, 0.01, 'select timeout'),

              MPSetting('altreadout', int, 10, 'Altitude Readout',
                        range=(0,100), increment=1, tab='Announcements'),
              MPSetting('distreadout', int, 200, 'Distance Readout', range=(0,10000), increment=1),

              MPSetting('moddebug', int, 0, 'Module Debug Level', range=(0,3), increment=1, tab='Debug'),
              MPSetting('compdebug', int, 0, 'Computation Debug Mask', range=(0,3), tab='Debug'),
              MPSetting('flushlogs', bool, False, 'Flush logs on every packet'),
              MPSetting('requireexit', bool, False, 'Require exit command'),
              MPSetting('wpupdates', bool, True, 'Announce waypoint updates'),

              MPSetting('basealt', int, 0, 'Base Altitude', range=(0,30000), increment=1, tab='Altitude'),
              MPSetting('wpalt', int, 100, 'Default WP Altitude', range=(0,10000), increment=1),
              MPSetting('rallyalt', int, 90, 'Default Rally Altitude', range=(0,10000), increment=1),
              MPSetting('terrainalt', str, 'Auto', 'Use terrain altitudes', choice=['Auto','True','False']),
              MPSetting('rally_breakalt', int, 40, 'Default Rally Break Altitude', range=(0,10000), increment=1),
              MPSetting('rally_flags', int, 0, 'Default Rally Flags', range=(0,10000), increment=1),

              MPSetting('source_system', int, 255, 'MAVLink Source system', range=(0,255), increment=1, tab='MAVLink'),
              MPSetting('source_component', int, 0, 'MAVLink Source component', range=(0,255), increment=1),
              MPSetting('target_system', int, -1, 'MAVLink target system', range=(-1,255), increment=1),
              MPSetting('target_component', int, 0, 'MAVLink target component', range=(0,255), increment=1)
            ])

        self.completions = {
            "script"         : ["(FILENAME)"],
            "set"            : ["(SETTING)"],
            "module"    : ["list",
                           "load (AVAILMODULES)",
                           "<unload|reload> (LOADEDMODULES)"]
            }

        self.status = MPStatus()

        # master mavlink device
        self.mav_master = None

        # mavlink outputs
        self.mav_outputs = []

        # SITL output
        self.sitl_output = None

        self.mav_param = mavparm.MAVParmDict()
        self.modules = []
        self.public_modules = {}
        self.functions = MAVFunctions()
        self.select_extra = {}
        self.continue_mode = False
        self.aliases = {}

    def module(self, name):
        '''Find a public module (most modules are private)'''
        if name in self.public_modules:
            return self.public_modules[name]
        return None
    
    def master(self):
        '''return the currently chosen mavlink master object'''
        if len(self.mav_master) == 0:
              return None
        if self.settings.link > len(self.mav_master):
            self.settings.link = 1

        # try to use one with no link error
        if not self.mav_master[self.settings.link-1].linkerror:
            return self.mav_master[self.settings.link-1]
        for m in self.mav_master:
            if not m.linkerror:
                return m
        return self.mav_master[self.settings.link-1]


class Opts(object):
    pass


class MyServant(object):
    def __init__(self):
        
        t = threading.Thread(target=mavproxy, name='mavproxy')
		t.daemon = True
		t.start()

#		No se si tengo que ejecutar este metodo
#        self.broker = wise.initialize(server="gunicorn")
        time.sleep(5)
        self.send_messages()

    def get_proxy(self):
        while 1:
            retval = self.broker.stringToProxy("obj1 -w browser")
            if retval:
                return retval

            print("Browser is not ready...")
            time.sleep(1)

        return self.broker.stringToProxy_wait

    def send_messages(self):
        proxy = self.get_proxy()
        while True:
            cadena = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(v.location.lat, v.location.lon, v.location.alt)
            response = proxy.upper(cadena)
            time.sleep(1)

    def sayHello(self, current):
        print "Hello!"
    
    def mavproxy(self):
        (opts, args) = parser.parse_args()    

        from pymavlink import mavutil, mavparm
        mavutil.set_dialect("ardupilotmega")

        mpstate = MPState()
        mpstate.status.exit = False
        mpstate.command_map = command_map
        mpstate.continue_mode = opts.continue_mode

        mpstate.completion_functions = {}    
        mpstate.settings.target_system = -1
        mpstate.settings.target_component = 0

        mpstate.mav_master = []
        mpstate.rl = rline.rline("MAV> ", mpstate)

        load_module('link', quiet=True)

        mpstate.settings.source_system = opts.SOURCE_SYSTEM
        mpstate.settings.source_component = opts.SOURCE_COMPONENT

        mdev='/dev/ttyUSB0'    
        assert mpstate.module('link').link_add(mdev)

        open_logs()

        mpstate.settings.streamrate = opts.streamrate
        mpstate.settings.streamrate2 = opts.streamrate

        msg_period = mavutil.periodic_event(1.0/15)
        heartbeat_period = mavutil.periodic_event(1)
        heartbeat_check_period = mavutil.periodic_event(0.33)

        mpstate.input_queue = Queue.Queue()
        mpstate.input_count = 0
        mpstate.empty_input_count = 0

        standard_modules = [
            'log', 'wp', 'rally','fence','param','relay','tuneopt',
            'arm','mode','calibration','rc','auxopt','misc','cmdlong',
            'battery','terrain','output','droneapi.module.api']

        for m in standard_modules:
            load_module(m, quiet=True)

        mpstate.status.thread = threading.Thread(target=main_loop, name='main_loop')
        mpstate.status.thread.daemon = True
        mpstate.status.thread.start()

        raw_input()

        modulo = mpstate.modules[-1][0]
        conexion = modulo.get_connection()
        vehiculo = conexion.get_vehicles()[0]

        print "Esperando a que se inicie el dron.."
        vehiculo.wait_init()
        print "Dron Activado"

        time.sleep(10)
        location = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(vehiculo.location.lat, vehiculo.location.lon, vehiculo.location.alt)
        print location



def initialize(broker):
    adapter = broker.createObjectAdapter("Adapter", "-w hello")
    adapter.add(MyServant(), "Hello")
