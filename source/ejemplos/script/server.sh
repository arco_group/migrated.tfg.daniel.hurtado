#!/bin/bash
# -*- mode: sh; coding: utf-8 -*-

PROJECT="dronheal"

# ------------------------------------------------------------

export DJANGO_SETTINGS_MODULE=$PROJECT.settings
export PYTHONPATH=$(pwd)

WORKER_CLASS=wise.guworkers.GeventWebSocketWorker
WSGI_APPLICATION=$PROJECT.wsgi:application

gunicorn \
    --debug \
    --daemon \
    --reload \
    --access-logfile - \
    --error-logfile - \
    --log-level info \
    --worker-class $WORKER_CLASS \
    $WSGI_APPLICATION
