# -*- mode: python; coding: utf-8 -*-

import time
import thread
import mavproxyapi

def get_proxy(broker):
    while 1:
        retval = broker.stringToProxy("printer1 -w browser")
        if retval:
            return retval

        print("Nevegador no está listo...")
        time.sleep(1)


def send_messages(broker):
    proxy = get_proxy(broker)
    for i in range(10):
        proxy.set_message("Hola, Mundo! ({})".format(i))
        time.sleep(1)


def initialize(broker):
    print ("Hola")
    #mavproxyapi.start()
    thread.start_new_thread(send_messages, (broker,))
    print ("Adios")
    #thread.start_new_thread(mavproxyapi.start, ())
