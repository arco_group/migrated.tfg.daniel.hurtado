// -*- mode: js; coding: utf-8 -*-

function prepend(id, text) {
    div = document.getElementById(id);
    div.innerHTML = text + div.innerHTML;
}

Servant = Class.extend({
    gps: function(message) {
	var retval = message.toUpperCase();
	prepend('messages', "upper('{0}'), retval: '{1}'<br/>".format(
	    message, retval));

	return retval;
    },
});

function wise_application(ws) {
    
    ws.createObjectAdapter("WebAdapter", "-w browser").then(on_adapter_ready)
	.then(wise.ready);

    function on_adapter_ready(adapter) {
	var proxy = adapter.add(new Servant(), "obj1");
	_("proxy:", proxy.wise_toString());
    }
}
