#!/bin/bash
# -*- mode: sh; coding: utf-8 -*-

PROJECT=django_hello

# ------------------------------------------------------------

PROJECT_DIR=/home/danicianuro/Prueba/wise
WORKER_CLASS=wise.guworkers.GeventWebSocketWorker

export PYTHONPATH=$PROJECT_DIR:/home/danicianuro/Dropbox/tfg.daniel.hurtado/source:$(pwd)
export DJANGO_SETTINGS_MODULE=$PROJECT.settings

gunicorn \
    --debug \
    --reload \
    --access-logfile - \
    --error-logfile - \
    --log-level info \
    --log-file - \
    --worker-class $WORKER_CLASS \
    $PROJECT.wsgi:application
