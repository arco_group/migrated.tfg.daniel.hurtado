# -*- mode: python; coding: utf-8 -*-

import time
import thread
import mavproxyapi

def get_proxy(broker):
    while 1:
        retval = broker.stringToProxy("printer1 -w browser")
        if retval:
            return retval

        print("Navegador no está listo...")
        time.sleep(1)


def send_messages(broker):
    proxy = get_proxy(broker)
    for i in range(10):
        posicion = enviarGPS()
        proxy.set_message(posicion + "Hola, Mundo! ({})".format(i))
        time.sleep(1)

def enviarGPS():
    time.sleep(30)
    modulo = mavproxyapi.mpstate.modules[-1][0]
    conexion = modulo.get_connection()
    vehiculo = conexion.get_vehicles()[0]
    #print modulo
    #print conexion
    #print vehiculo
    #time.sleep(10)
    #print "Esperando a que se inicie el dron.."
    ##vehiculo.wait_init()
    #time.sleep(10)
    #print "Dron Activado"
    #print vehiculo.attitude.pitch
    location = "lat:  '{}';  lon:  '{}';  alt:  '{}'".format(vehiculo.location.lat, vehiculo.location.lon, vehiculo.location.alt)
    return location

def initialize(broker):
    print ("Hola")
    #mavproxyapi.start()
    thread.start_new_thread(send_messages, (broker,))
    print ("Adios")
    #thread.start_new_thread(enviarGPS, ())
    #thread.start_new_thread(mavproxyapi.start, ())
