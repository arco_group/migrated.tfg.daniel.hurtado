# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mapa', '0004_auto_20150526_1848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='fecha_creacion',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 26, 18, 50, 14, 685424), verbose_name=b'fecha de envio'),
            preserve_default=True,
        ),
    ]
