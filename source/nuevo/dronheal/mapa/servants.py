# -*- mode: python; coding: utf-8 -*-

import time
import thread


def get_proxy(broker):
    while 1:
        retval = broker.stringToProxy("printer1 -w browser")
        if retval:
            return retval

        print("Navegador no esta listo...EO")
        time.sleep(1)

def send_messages(broker):
    proxy = get_proxy(broker)
    for i in range(10):
        response = proxy.upper("Hola, Mundo! ({})".format(i))
        print("El Navegador dice: {}".format(response))
        time.sleep(1)

def initialize(broker):
    time.sleep(5)
    thread.start_new_thread(send_messages, (broker,))
