#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.core.urlresolvers import reverse
from mapa.models import *
from django.views.generic import ListView
import datetime

# Create your views here.

def index(request):
    return render(request, "index.html")

@login_required
def mapa(request):
	if request.user.is_superuser:
		return HttpResponseRedirect('/admin/')

	operarios = Group.objects.last()
	grupo_usuario = request.user.groups.get()

	if grupo_usuario == operarios:
		url = '/operacion/'
	else:
		url = '/admin/'

	return HttpResponseRedirect(url)

@login_required
def operacion(request):
	clientes=Cliente.objects.order_by('nombre')
	drones=Drone.objects.order_by('nombre')
	central=Central.objects.order_by('nombre')
	form = EnvioForm()
	return render_to_response('mapa.html', {'form':form,'clientes': clientes,'drones': drones,'central':central})


class EnvioList(ListView):

	model = Envio

@login_required
def add_envio(request):
	#Si el usuario esta mandando el formulario con los datos
	if request.method == 'POST':
		form = EnvioForm(request.POST)
		#Comprobamos que el formulario es correcto
		if form.is_valid():
			#Guardamos los datos en la base de datos
			new_envio = form.save()

			return HttpResponseRedirect(reverse('operacion'))
	else:
		form = EnvioForm()

	return render(request, 'mapa/envio_form.html', {'form':form})
